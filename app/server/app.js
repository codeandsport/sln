// base 3rd party app
const express = require("express");
const http = require("http");
const path = require("path");
const mongoose = require("mongoose");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const session= require("express-session");
const flash = require("connect-flash");
const routes = require("./routes/routers");
const passport = require("passport");
let app = express();
// configurations
mongoose.connect("mongodb://localhost:27017/sln", { useNewUrlParser: true });

app.set("views", path.join(__dirname, "./views"));
app.set("view engine", "ejs");
app.use(express.static("public"));

app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(flash());
app.use(cookieParser());
app.use(session({
    secret: "TKRv0IJs=HYqrvagQ#&!F!%V]Ww/4KiVs$s,<<MX",
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(routes);

module.exports = app;
