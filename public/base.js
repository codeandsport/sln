	// When the user scrolls down 20px from the top of the document, show the button
	// console.log(window);
	window.onscroll = function() {
	    scrollFunction()
	};

	function scrollFunction() {
	    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
	        document.getElementById("btt").style.display = "block";
	    } else {
	        document.getElementById("btt").style.display = "none";
	    }
	}

	// When the user clicks on the button, scroll to the top of the document
	function topFunction() {
	    document.body.scrollTop = 0;
	    document.documentElement.scrollTop = 0;
	}

	function unfoldfn() {
	    var btn = document.getElementById("navbtn");
	    if (btn.className === "buttons") {
	        btn.className = "responsive";
	    } else {
	        btn.className = "buttons";
	    }
	}
